function isSubjectExists() {
    let subject = document.getElementById("subject");
    let helpSubjectExists = document.getElementById("helpSubjectExists");

    $.ajax({

        url:'controllers/issue.php',
        dataType: 'json',
        
        data:{
            'is_subject_exists': subject.value,
        },    
        type:'POST',
        success: function(response) {       
            
         if(response[1].is_subject_exists){
            helpSubjectExists.innerHTML = 'subject field should be unique';
            subject.value = '';
         }else{
            helpSubjectExists.innerHTML = '';
         }
        }
    
    });
  }



  