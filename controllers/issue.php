<?php
require_once '../includes/config.php';
if (isset($_POST['issue_submit'])) {
    $data = [];
    $data['subject'] = $_POST['subject'];
    $data['description'] = $_POST['description'];
    $data['status'] = $_POST['status'];
    $data['priority'] = $_POST['priority'];
    $data['due_date'] = date('Y-m-d', strtotime($_POST['due_date']));
    $data['assignee_id'] = $_POST['assignee_id'];
    $data['reviewer_id'] = $_POST['reviewer_id'];
    $data['target_version_id'] = $_POST['target_version_id'];
    $data['reviewer_comments'] = $_POST['reviewer_comments'];

    $obj = new Issue();
    $obj->save($data);
    if ($obj->getErrorMsg()) {
        echo $obj->getErrorMsg();
    } else {
        redirectTo("Location:/index.php");
    }
} elseif (isset($_POST['issue_submit_edit']) && $_POST['id']) {
    $issue_id = (int) $_POST['id'];
    if ($issue_id) {
        $data = [];
        $data['subject'] = $_POST['subject'];
        $data['description'] = $_POST['description'];
        $data['status'] = $_POST['status'];
        $data['priority'] = $_POST['priority'];
        $data['due_date'] = date('Y-m-d', strtotime($_POST['due_date']));
        $data['assignee_id'] = $_POST['assignee_id'];
        $data['reviewer_id'] = $_POST['reviewer_id'];
        $data['target_version_id'] = $_POST['target_version_id'];
        $data['reviewer_comments'] = $_POST['reviewer_comments'];

        $obj = new Issue();
        $obj->update($issue_id, $data);
        if ($obj->getErrorMsg()) {
            echo $obj->getErrorMsg();
        } else {
            redirectTo("Location:/index.php");
        }
    } else {
        echo 'Invalid ID';
    }
} else {
    if (isset($_POST['delete_list'])) {

        $delete_list = implode(",", $_POST['delete_list']);

        $obj = new Issue();
        $obj->delete($delete_list);

        if ($obj->getErrorMsg()) {
            echo $obj->getErrorMsg();
        } else {
            redirectTo("Location:/index.php");
        }
    } elseif (isset($_POST['is_subject_exists'])) {
        $subject = $_POST['is_subject_exists'];

        if ($subject) {
            $obj = new Issue();
            $exists = $obj->isSubjectExists($subject);

            $data['subject'] = $subject;
            $data['is_subject_exists'] =  $exists;


            if ($obj->getErrorMsg()) {
                echo json_encode(['fail', $data, $obj->getErrorMsg()]);
            } else {
                echo json_encode(['success', $data]);
            }
        } else {
            echo json_encode(['empty', $data]);
        }
    } else {
        redirectTo("Location:/index.php");
    }
}
