CREATE DATABASE `issue-scrutinize` CHARACTER SET utf8;

CREATE TABLE `issue-scrutinize`.`issue` ( `id` INT(10) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT ,  
 `subject` VARCHAR(255) NOT NULL ,  `description` TINYTEXT NOT NULL ,  
 `status` ENUM('New','Assigned','In-Progress','Under-review','Closed') NOT NULL , 
 `priority` ENUM('High','Medium','Low') NOT NULL ,  `due_date` DATE NOT NULL ,  
 `assignee_id` INT NOT NULL ,  `reviewer_id` INT NOT NULL ,  `target_version` VARCHAR(10) NOT NULL ,  
 `reviewer_comments` VARCHAR(255) NOT NULL , 
 `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
 `updated_at` TIMESTAMP on update CURRENT_TIMESTAMP NOT NULL,
 `deleted_at` TIMESTAMP NULL DEFAULT NULL ,
  PRIMARY KEY  (`id`),    UNIQUE  `subject` (`subject`)) ENGINE = InnoDB;

CREATE TABLE `issue-scrutinize`.`image` ( `id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,  `alt` VARCHAR(255) NOT NULL ,  `url` VARCHAR(255) NOT NULL ,  `issue_id` INT UNSIGNED NOT NULL ,    PRIMARY KEY  (`id`)) ENGINE = InnoDB;

CREATE TABLE `issue-scrutinize`.`user` ( `id` INT NOT NULL AUTO_INCREMENT ,  `position` VARCHAR(255) NOT NULL DEFAULT 'developer' ,  `name` VARCHAR(255) NOT NULL ,    PRIMARY KEY  (`id`),    UNIQUE  `name` (`name`)) ENGINE = InnoDB;

CREATE TABLE `issue-scrutinize`.`affected_region` ( `id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,  `issue_id` INT NOT NULL ,  `region_id` INT NOT NULL ,    PRIMARY KEY  (`id`)) ENGINE = InnoDB;

CREATE TABLE `issue-scrutinize`.`region` ( `id` INT NOT NULL AUTO_INCREMENT , `name` VARCHAR(255) NOT NULL , PRIMARY KEY (`id`), UNIQUE `name` (`name`)) ENGINE = InnoDB; 

ALTER TABLE `issue` CHANGE `target_version` `target_version_id` INT(10) NOT NULL;

CREATE TABLE `issue-scrutinize`.`version` ( `id` INT NOT NULL AUTO_INCREMENT ,  `version_name` VARCHAR(20) NOT NULL ,  `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,  `updated_at` TIMESTAMP on update CURRENT_TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,    PRIMARY KEY  (`id`),    UNIQUE  `version_name` (`version_name`)) ENGINE = InnoDB;