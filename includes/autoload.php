<?php

function autoload($class_name)
{
    $file = ROOT . 'models/' . $class_name . '.class.php';
    if (file_exists($file) == true) {
        include_once $file;
    }
}
spl_autoload_register('autoload');
