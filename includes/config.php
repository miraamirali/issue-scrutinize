<?php

/**
 * Softdel
 *
 * @author  Mir Aamir Ali <aamiralimir12@gmail.com>
 */

define('PROJECT_START', microtime(true));

error_reporting(E_ALL);
ini_set("display_errors", 1);

session_start();



//set timezone
date_default_timezone_set('Asia/Calcutta');


//application address
define('ROOT', dirname(__FILE__, 2) . '/');
define('SITEEMAIL', 'aamiralimir12@gmail.com');

require_once ROOT . 'includes/autoload.php';

$title = 'Issue Scrutinize';

function redirectTo($loc) {
        header( 'Content-type: text/html; charset=utf-8' ); // make sure this is set
        header($loc, true, 307 ); // 307 is temporary redirect
        echo "<html></html>";  // - Tell the browser there the page is done
        exit;                  // - Prevent any more output from messing up the redirect
    }
