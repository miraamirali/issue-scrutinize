<?php
$issue_id = isset($_GET['issue_id'])? (int) $_GET['issue_id']: false;
$mode = 'Add Issue';
$obj_user = new User();
$users = $obj_user->get();
$obj_region = new Region();
$regions = $obj_region->get();
$obj_version = new Version();
$versions = $obj_version->get();

if ($issue_id) {
    $obj_issue = new Issue();
    $issue = $obj_issue->get( $issue_id);
    $mode = $issue ? 'Edit Issue' : 'Add Issue';
}elseif($_SERVER['PHP_SELF'] == '/index.php'){
    $obj = new Issue();
    $issues = $obj->get();
    $user_ids=[];
    foreach($users as  $user){
        $user_ids[$user['id']] = $user['name'];
    }
    $version_ids = [];
    foreach($versions as  $version){
        $version_ids[$version['id']] = $version['version_name'];
    }
}