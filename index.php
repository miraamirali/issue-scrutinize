<?php require_once 'includes/config.php'; ?>
<!DOCTYPE html>
<html lang="en">
<?php include_once 'view/layouts/header.php'; ?>

<body class="nav-md">
    <div class="container body">
        <div class="main_container">
            <!-- left-col -->
            <?php include_once 'view/layouts/left-col.php' ?>
            <!-- /left-col -->

            <!-- top navigation -->
            <?php include_once 'view/layouts/menu.php' ?>
            <!-- /top navigation -->

            <!-- page content -->
            <div class="right_col" role="main">
            <?php include_once ROOT . 'includes/data_inputs.php'; ?>

                <div class="clearfix"></div>

                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <form id="issue-list-form" method="post" action="controllers/issue.php">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2>Issues</small></h2>
                                    <ul class="nav navbar-right panel_toolbox">
                                        <li><a href="#" onclick="document.getElementById('issue-list-form').submit();" class="close-add"><i class="fa fa-remove"></i> Delete</a>
                                        <li><a href="/issue.php" class="close-add"><i class="fa fa-plus"></i> Add</a>
                                        </li>
                                    </ul>
                                    <div class="clearfix"></div>
                                </div>

                                <div class="x_content">
                                    <!-- Issues List -->
                                    <?php include_once 'view/tables/issues.php' ?>
                                    <!-- /tIssues List -->
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- /page content -->

            <?php include_once 'view/layouts/footer.php' ?>
        </div>
    </div>
    <?php include_once ROOT . 'view/includes/js.php' ?>

</body>

</html>