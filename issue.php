<?php require_once 'includes/config.php'; ?>

<!DOCTYPE html>
<html lang="en">
<?php include_once 'view/layouts/header.php'; ?>

<body class="nav-md">
    <div class="container body">
        <div class="main_container">
            <!-- left-col -->
            <?php include_once 'view/layouts/left-col.php'; ?>
            <!-- /left-col -->

            <!-- top navigation -->
            <?php include_once 'view/layouts/menu.php'; ?>
            <!-- /top navigation -->

            <!-- page content -->
            <div class="right_col" role="main">
                <?php include_once ROOT . 'includes/data_inputs.php'; ?>

                <div class="clearfix"></div>

                <div class="row">
                    <div class="col-md-6 col-md-offset-3 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_title">
                                <h2><?=$mode?></h2>
                                <div class="clearfix"></div>
                            </div>

                            <div class="x_content">
                                <!-- /page content -->
                                <?php include_once 'view/forms/add-issue.php'; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /page content -->
            <?php include_once 'view/layouts/footer.php'; ?>
        </div>
    </div>

    <?php include_once ROOT . 'view/includes/js.php'; ?>
    <script src="assets/js/issue-scrutinize.js"></script>

</body>
</html>