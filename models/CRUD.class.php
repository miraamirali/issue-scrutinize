<?php

class CRUD
{
	private static $_instance = null;

	private $db_host = 'mysql57', $db_port = '3306', $db_database = 'issue-scrutinize', $db_username = 'root', $db_password = 'root';
	private $_mysqli, $_query, $_error = false, $_error_msg = "", $_results = array(), $_insert_id = 0, $_affected_rows, $_num_rows = 0;
	public function __construct()
	{
		$this->_mysqli = $this->_connection();
	}

	/**
	 * destroys mysql connection and destroys current instance  only if instance is not null
	 */
	public function __destruct()
	{
		if (!is_null(self::$_instance) && !is_null($this->_mysqli)) {
			$this->_mysqli->close();
			$this->_mysqli = null;
			self::$_instance = null;
		}
	}
	private function _connection()
	{
		$this->_mysqli = new mysqli($this->db_host, $this->db_username, $this->db_password, $this->db_database);
		/* check connection */
		if ($this->_mysqli->connect_error) {
			die('Connect Error (' . $this->_mysqli->connect_errno . ') '
					. $this->_mysqli->connect_error);
		}
	}
	public static function getInstance()
	{
		if (!isset(self::$_instance)) {
			self::$_instance = new CRUD();
		}
		return self::$_instance;
	}
	public function escapeString($value)
	{
		return $this->_mysqli->real_escape_string(trim($value));
	}
	private function _reset_vars()
	{
		$this->_query = "";
		$this->_error = false;
		$this->_error_msg = "";
		$this->_results = array();
		$this->_insert_id = 0;
		$this->_affected_rows;
		$this->_num_rows = 0;
		if (!$this->_mysqli) {
			$this->_connection();
		}
		return true;
	}
	public function select($table, $fields = "*", $joins = null, $where = null, $order = null, $limit = array())
	{
		$this->_reset_vars();
		$this->_query = "SELECT $fields FROM $table $joins $where $order " . (!empty($limit) ? "LIMIT $limit[0],$limit[1]" : "");

		if ($res = $this->_mysqli->query($this->getQuery())) {
			$this->_num_rows = $res->num_rows;
			while ($row = $res->fetch_assoc()) {
				array_push($this->_results, $row);
			}
			return true;
		} else {
			$this->_error = $this->_mysqli->errno;
			$this->_error_msg = $this->_mysqli->error;
		}
		return false;
	}
	public function update($table, $fields_values = array(), $where)
	{
		$this->_reset_vars();
		if (count($fields_values) > 0) {
			$fields_val = "";
			foreach ($fields_values as $field => $value) {
				$value = $this->escapeString($value);
				$fields_val .= "`$field`='$value',";
			}
			$fields_val = rtrim($fields_val, ",");
			$this->_query = "UPDATE $table SET $fields_val $where";
			if ($this->_mysqli->query($this->getQuery())) {
				$this->_affected_rows = $this->_mysqli->affected_rows;
				return true;
			} else {
				$this->_error = $this->_mysqli->errno;
				$this->_error_msg = $this->_mysqli->error;
			}
		}
		return false;
	}
	public function insert($table, $fields_values = array())
	{
		$this->_reset_vars();
		if (count($fields_values) > 0) {
			$fields = "";
			$values = "";
			foreach ($fields_values as $field => $value) {
				$fields .= "`$field`,";
				$value = $this->escapeString($value);
				$values .= "'$value',";
			}
			$fields = rtrim($fields, ",");
			$values = rtrim($values, ",");

			$this->_query = "INSERT INTO $table($fields) VALUES($values)";
			if ($this->_mysqli->query($this->getQuery())) {
				$this->_insert_id = $this->_mysqli->insert_id;
				return true;
			} else {
				$this->_error = $this->_mysqli->errno;
				$this->_error_msg = $this->_mysqli->error;
			}
		}
		return false;
	}
	public function delete($table, $where, $order = null, $limit = array())
	{
		$this->_reset_vars();
		$this->_query = "DELETE FROM $table $where $order " . (!empty($limit) ? "Limit $limit[0],$limit[1]" : "");
		if ($this->_mysqli->query($this->getQuery())) {
			$this->_affected_rows = $this->_mysqli->affected_rows;
			return true;
		} else {
			$this->_error = $this->_mysqli->errno;
			$this->_error_msg = $this->_mysqli->error;;
		}
		return false;
	}
	public function getQuery()
	{
		return $this->_query;
	}
	public function getError()
	{
		return $this->_error;
	}
	public function getErrorMsg()
	{
		return $this->_error_msg;
	}
	public function getResults()
	{
		return $this->_results;
	}
	public function getFirst()
	{
		return $this->_results[0];
	}
	public function getInsertId()
	{
		return $this->_insert_id;
	}
	public function getAffectedRows()
	{
		return $this->_affected_rows;
	}
	public function getNumRows()
	{
		return $this->_num_rows;
	}
}
