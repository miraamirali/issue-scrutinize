<?php
class Issue
{
    private $_db, $_data, $_error, $_query;
    public function __construct()
    {
        $this->_table = 'issue';
        $this->_db = CRUD::getInstance();
    }
    public function getTableName()
    {
        return $this->_table;
    }
    public function  __destruct()
    {
        $this->_db->__destruct();
    }
    public function getData()
    {
        return $this->_data;
    }
    public function getDBConnection()
    {
        return $this->_db;
    }
    public function get($id = null, $order_by = 'created_at', $soft_deletes = false)
    {
        $where_cond = [];
        if ($soft_deletes == false) {
            $where_cond[] = " deleted_at IS NULL";
        }
        if ($id != null) {
            $where_cond[] = " id={$id}";
        }
        $order_cond = ' ORDER BY ' . $order_by . '  DESC';
        $where_cond = count($where_cond) ? " WHERE " . implode(' AND ', $where_cond) : '';
        $data = $this->_db->select($this->_table, "*", "", $where_cond, $order_cond);
        if ($data && $id) {
            return $this->_data = $this->_db->getFirst();
        }
        if ($data && $this->_db->getNumRows() > 0) {
            return $this->_data = $this->_db->getResults();
        }
        return [];
    }
    public function save($data)
    {
        $data = $this->_db->insert($this->_table, $data);
        if ($data) {
            $this->_insert_id = $this->_db->getInsertId();
            $this->_query = $this->_db->getQuery();
            return true;
        }
        $this->_error = $this->_db->getErrorMsg();
        return false;
    }
    public function update($id, $data)
    {
        $where = "WHERE id={$id}";
        $data = $this->_db->update($this->_table, $data, $where);
        if ($data) {
            $this->_insert_id = $this->_db->getInsertId();
            $this->_query = $this->_db->getQuery();
            return true;
        }
        $this->_error = $this->_db->getErrorMsg();
        return false;
    }
    public function delete($list)
    {
        $where = " WHERE id IN (  {$list} ) ";
        $data = $this->_db->delete($this->_table, $where);
        if ($data) {
            $this->_insert_id = $this->_db->getInsertId();
            $this->_query = $this->_db->getQuery();
            return true;
        }
        $this->_error = $this->_db->getErrorMsg();
        return false;
    }
    public function isSubjectExists($subject)
    {
        $where_cond = " WHERE subject LIKE '$subject'";
        $data = $this->_db->select($this->_table, "*", "", $where_cond);

        if ($data && $this->_db->getNumRows() > 0) {
            return true;
        }
        return false;
    }
    public function getErrorMsg()
    {
        return $this->_error;
    }
    public function getQuery()
    {
        return $this->_query;
    }
}
