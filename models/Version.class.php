<?php
class Version
{
    private $_db, $_data;
    public function __construct()
    {
        $this->_table = 'version';
        $this->_db = CRUD::getInstance();
    }
    public function getTableName()
    {
        return $this->_table;
    }
    public function  __destruct()
    {
        $this->_db->__destruct();
    }
    public function getData()
    {
        return $this->_data;
    }
    public function getDBConnection()
    {
        return $this->_db;
    }
    public function get($id = null)
    {
        $where_cond = [];
        if ($id != null) {
            $where_cond[] = " id={$id}";
        }
        $where_cond = count($where_cond) ? " WHERE " . implode(' AND ', $where_cond) : '';
        $data = $this->_db->select($this->_table, "*", "", $where_cond);
        if ($data && $this->_db->getNumRows() > 0) {
            return $this->_data = $this->_db->getResults();
        }
        return [];
    }
}