##Setup Instruction
- 
- Using Laradock
- - stop local machine apache `sudo service apache2 stop`
- - stop local machine mysql `sudo service mysql stop`
- - install docker and docker compose on ubuntu 16.04 using the [doc](https://docs.docker.com/compose/install/#install-compose) [doc1](https://docs.docker.com/install/linux/docker-ce/ubuntu/#install-docker-ce-1)
- - start docker
- - Install laradock for blank project as mentioned in [instalation doc](http://laradock.io/getting-started/#installation)

- - add new issuescrutinize.test config in 'laradockroot/apache2/sites/issuescrutinize.test.conf' as
``` 
<VirtualHost *:80>
  ServerName issuescrutinize.test
  DocumentRoot /var/www/issuescrutinize/
  Options Indexes FollowSymLinks

  <Directory "/var/www/issuescrutinize/">
    AllowOverride All
    <IfVersion < 2.4>
      Allow from all
    </IfVersion>
    <IfVersion >= 2.4>
      Require all granted
    </IfVersion>
  </Directory>

</VirtualHost> 
```
- - start ur docker instance from within the lardock root as `sudo docker-compose up -d apache2 mysql57`
- - on successful docer instance, connect to the bash using `sudo docker-compose exec workspace bash`
- - u will be connected to docker ssh instance in `/var/www` path

### if u Using Local Machine: Go though below steps

- - clone this repo as a folder under the above laradock folder structure. `git clone https://miraamirali@bitbucket.org/miraamirali/issue-scrutinize.git`
- - move to path `/var/www/issue-scrutinize/`
- - add issuescrutinize.test in local machine /etc/hosts file

- - Create Database `CREATE DATABASE `issue-scrutinize` CHARACTER SET utf8;`
- - access url issuescrutinize.test on browser


## About Issue Scrutinize

Web based platform that allows a user to manage their Issues list.


## Author 
Mir Aamir `aamiralimir12@gmail.com`
## License

The Laravel framework is open-source software licensed under the [MIT license](https://opensource.org/licenses/MIT).
