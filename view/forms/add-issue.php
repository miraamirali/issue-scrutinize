<form class="form-horizontal form-label-left" method="post" action="controllers/issue.php">
    <div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12">Subject*</label>
        <div class="col-md-9 col-sm-9 col-xs-12">
            <input id="subject" onfocusout="isSubjectExists()" type="text" name="subject" class="form-control" placeholder="Subject" value="<?php echo ($mode=='Edit Issue')?  $issue['subject']: '' ?>" required>
            <span id="helpSubjectExists" class="text-danger"></span>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12">Description</label>
        <div class="col-md-9 col-sm-9 col-xs-12">
            <textarea name="description" class="form-control" rows="3" placeholder="Description"><?php echo ($mode=='Edit Issue')?  $issue['description']: '' ?></textarea>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12">Status*</label>
        <div class="col-md-9 col-sm-9 col-xs-12">
            <select name="status" class="form-control" required>
                <option value="New" <?php echo ($mode=='Edit Issue' && $issue['status'] == 'New')?  'selected': '' ?>>New</option>
                <option value="Assigned" <?php echo ($mode=='Edit Issue' && $issue['status'] == 'Assigned')?  'selected': '' ?>>Assigned</option>
                <option value="In-Progress" <?php echo ($mode=='Edit Issue' && $issue['status'] == 'In-Progress')?  'selected': '' ?>>In-Progress</option>
                <option value="Under-review" <?php echo ($mode=='Edit Issue' && $issue['status'] == 'Under-review')?  'selected': '' ?>>Under-review</option>
                <option value="Closed" <?php echo ($mode=='Edit Issue' && $issue['status'] == 'Closed')?  'selected': '' ?>>Closed</option>
            </select>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 col-sm-3 col-xs-12 control-label">Priority*
        </label>

        <div class="col-md-9 col-sm-9 col-xs-12">
            <div class="radio">
                <label>
                    <input type="radio" class="flat" name="priority" value="High" <?php echo ($mode=='Edit Issue' && $issue['priority'] == 'High')?  'checked': '' ?>> High
                </label>
            </div>
            <div class="radio">
                <label>
                    <input type="radio" class="flat" name="priority" value="Medium" <?php echo ($mode=='Edit Issue' && $issue['priority'] == 'Medium')?  'checked': '' ?>> Medium
                </label>
            </div>
            <div class="radio">
                <label>
                    <input type="radio" class="flat" name="priority" value="Low" <?php echo ($mode=='Edit Issue' && $issue['priority'] == 'Low')?  'checked': '' ?>> Low
                </label>
            </div>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 col-sm-3 col-xs-12 control-label">
            Affected Regions
        </label>

        <div class="col-md-9 col-sm-9 col-xs-12">
            <?php foreach ($regions as $region) { ?>
                <div class="radio">
                    <label>
                        <input type="checkbox" class="flat" name="affected_regions_list[]" value="<?php echo $region['id']; ?>" checked> <?php echo $region['name']; ?>
                    </label>
                </div>
            <?php } ?>
        </div>
    </div>

    <div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12">Due Date <span class="required">*</span>
        </label>
        <div class="col-md-9 col-sm-9 col-xs-12">
            <input name="due_date" type="text" id="duedate" class="form-control" placeholder="Due Date" value="<?php echo ($mode=='Edit Issue')?  date('d-m-Y', strtotime($issue['due_date'])): '' ?>" required>
        </div>
    </div>

    <div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12">Assignee*</label>
        <div class="col-md-9 col-sm-9 col-xs-12">
            <select name="assignee_id" class="form-control">
                <?php foreach ($users as $user) { ?>
                    <option value="<?php echo $user['id']; ?>" <?php echo ($mode=='Edit Issue' && $issue['assignee_id'] == $user['id'])?  'selected': '' ?> ><?php echo $user['name']; ?></option>
                <?php } ?>
            </select>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12">Reviewer*</label>
        <div class="col-md-9 col-sm-9 col-xs-12">
            <select name="reviewer_id" class="form-control">
                <?php foreach ($users as $user) { ?>
                    <option value="<?php echo $user['id']; ?>" <?php echo ($mode=='Edit Issue' && $issue['reviewer_id'] == $user['id'])?  'selected': '' ?>><?php echo $user['name']; ?></option>
                <?php } ?>
            </select>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12">Target Version*</label>
        <div class="col-md-9 col-sm-9 col-xs-12">
            <select name="target_version_id" class="form-control">
                <?php foreach ($versions as $version) { ?>
                    <option value="<?php echo $version['id']; ?>"  <?php echo ($mode=='Edit Issue' && $issue['target_version_id'] == $version['id'])?  'selected': '' ?>><?php echo $version['version_name']; ?></option>
                <?php } ?>
            </select>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12">Images*</label>
        <div class="col-md-9 col-sm-9 col-xs-12">
            <input type="file" multiple class="form-control" placeholder="Images" required>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12">Reviewer Comments</label>
        <div class="col-md-9 col-sm-9 col-xs-12">
            <textarea name="reviewer_comments" class="form-control" rows="3" placeholder="Reviwer Comments"><?php echo ($mode=='Edit Issue')?  $issue['reviewer_comments']: '' ?></textarea>
        </div>
    </div>
    <div class="ln_solid"></div>
    <div class="form-group">
        <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
            <button type="button" class="btn btn-primary">Cancel</button>
            <button type="reset" class="btn btn-primary">Reset</button>
            <button type="submit" class="btn btn-success" name="<?php echo ($mode=='Edit Issue')? 'issue_submit_edit': 'issue_submit' ?>">Submit</button>
        </div>
    </div>
    <?php echo ($mode=='Edit Issue')? "<input type='hidden' name='id' value= {$issue['id']}>" : ''; ?>
</form>