<!-- Bootstrap -->
<link href="assets/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<!-- Font Awesome -->
<link href="assets/font-awesome/css/font-awesome.min.css" rel="stylesheet">

<!-- iCheck -->
<link href="assets/iCheck/skins/flat/green.css" rel="stylesheet">

<link href="assets/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" rel="stylesheet">

<!-- Custom Theme Style -->
<link href="assets/css/custom.css" rel="stylesheet">