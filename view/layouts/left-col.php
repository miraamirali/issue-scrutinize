<div class="col-md-3 left_col">
    <div class="left_col scroll-view">
        <div class="navbar nav_title" style="border: 0;">
            <a href="/" class="site_title"><span><?php echo $title; ?></span></a>
        </div>

        <div class="clearfix"></div>

        <!-- sidebar menu -->
        <?php include_once 'view/layouts/sidebar.php' ?>
        <!-- /sidebar menu -->

    </div>
</div>