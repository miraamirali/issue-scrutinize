<div class="table-responsive">
    <table class="table table-striped jambo_table">
        <thead>
            <tr class="headings">
                <th>
                    <!-- <input type="checkbox" id="check-all" class="flat"> -->
                </th>
                <th class="column-title">Issue Id </th>
                <th class="column-title">Subject </th>
                <th class="column-title">Status </th>
                <th class="column-title">Priority </th>
                <th class="column-title">Due Date </th>
                <th class="column-title">Assignee </th>
                <th class="column-title">Reviewer</th>
                <th class="column-title">Target Version</th>
            </tr>
        </thead>
        <tbody>
            <?php $i = 0; ?>
            <?php foreach($issues as $issue) { ?>
            <tr class="<?php echo ++$i% 2 ? 'even' : 'odd'; ?></td> pointer">
                <td class="a-center ">
                    <input type="checkbox" name="delete_list[]" value="<?php echo $issue['id']; ?>" class="flat" name="table_records">
                </td>
                <td class=" ">
                    <a title="Click Edit <?php echo 'CR' . $issue['id']; ?>" href="<?php echo '/issue.php?issue_id='.$issue['id']; ?>"><?php echo 'CR' . $issue['id']; ?></a>
                </td>
                <td class=" ">
                    <a title="Click Edit <?php echo 'CR' . $issue['id']; ?>" href="<?php echo '/issue.php?issue_id='.$issue['id']; ?>"><?php echo $issue['subject']; ?></a>
                </td>
                <td class=" ">
                    <a title="Click Edit <?php echo 'CR' . $issue['id']; ?>" href="<?php echo '/issue.php?issue_id='.$issue['id']; ?>"><?php echo $issue['status']; ?></a>
                </td>
                <td class=" ">
                    <a title="Click Edit <?php echo 'CR' . $issue['id']; ?>" href="<?php echo '/issue.php?issue_id='.$issue['id']; ?>"><?php echo $issue['priority']; ?></a>
                </td>
                <td class=" ">
                    <a title="Click Edit <?php echo 'CR' . $issue['id']; ?>" href="<?php echo '/issue.php?issue_id='.$issue['id']; ?>"><?php  echo date('d-M-Y', strtotime($issue['due_date'])); ?></a>
                </td>
                <td class=" ">
                    <a title="Click Edit <?php echo 'CR' . $issue['id']; ?>" href="<?php echo '/issue.php?issue_id='.$issue['id']; ?>"><?php echo $user_ids[$issue['assignee_id']]; ?></a>
                </td>
                <td class=" ">
                    <a title="Click Edit <?php echo 'CR' . $issue['id']; ?>" href="<?php echo '/issue.php?issue_id='.$issue['id']; ?>"><?php echo $user_ids[$issue['reviewer_id']]; ?></a>
                </td>
                <td class=" last">
                    <a title="Click Edit <?php echo 'CR' . $issue['id']; ?>" href="<?php echo '/issue.php?issue_id='.$issue['id']; ?>"><?php echo $version_ids[$issue['target_version_id']]; ?></a>
                </td>
            </tr>
            <?php } ?>          
        </tbody>
    </table>
</div>